package com.coreteka.pmaudiorecorder.di.modules;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.coreteka.pmaudiorecorder.core.db.FileDao;
import com.coreteka.pmaudiorecorder.core.db.PMAudioDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {

    @Provides
    @Singleton
    public PMAudioDatabase provideDatabase(Context context) {
        return Room.databaseBuilder(context.getApplicationContext(),
                                    PMAudioDatabase.class,
                                    "pmaudio-database")
                   .fallbackToDestructiveMigration()
                   .build();
    }

    @Provides
    @Singleton
    public FileDao provideCountryDao(PMAudioDatabase db) {
        return db.getFileDao();
    }

}
