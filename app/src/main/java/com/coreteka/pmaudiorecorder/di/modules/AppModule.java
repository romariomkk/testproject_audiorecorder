package com.coreteka.pmaudiorecorder.di.modules;

import android.app.Application;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module(includes = {RepoModule.class, StorageModule.class, NetworkModule.class, DatabaseModule.class})
public class AppModule {

    @Provides
    Context provideContext(Application application) {
        return application.getApplicationContext();
    }

}
