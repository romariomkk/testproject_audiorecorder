package com.coreteka.pmaudiorecorder.di.modules;

import com.coreteka.pmaudiorecorder.di.PresenterKey;
import com.coreteka.pmaudiorecorder.util.mvp.BasePresenter;
import com.coreteka.pmaudiorecorder.util.mvp.DaggerPresenterFactory;
import com.coreteka.pmaudiorecorder.util.mvp.PresenterFactory;
import com.coreteka.pmaudiorecorder.view.screens.audio.RecordPresenter;
import com.coreteka.pmaudiorecorder.view.screens.login.LoginPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class PresenterModule {

    @Binds
    abstract PresenterFactory bindPresenterFactory(DaggerPresenterFactory daggerPresenterFactory);

    @Binds
    @IntoMap
    @PresenterKey(LoginPresenter.class)
    abstract BasePresenter bindLoginPresenter(LoginPresenter loginPresenter);

    @Binds
    @IntoMap
    @PresenterKey(RecordPresenter.class)
    abstract BasePresenter bindRecordPresenter(RecordPresenter RecordPresenter);


}
