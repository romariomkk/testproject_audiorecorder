package com.coreteka.pmaudiorecorder.di.modules;

import android.content.Context;

import com.coreteka.pmaudiorecorder.core.storage.AuthStorage;
import com.coreteka.pmaudiorecorder.core.storage.GeneralDataStorage;
import com.coreteka.pmaudiorecorder.core.storage.PrefStorageImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class StorageModule {

    @Provides
    @Singleton
    AuthStorage provideAuthStorage(Context context) {
        return new PrefStorageImpl(context);
    }

    @Provides
    @Singleton
    GeneralDataStorage provideGeneralDataStorage(Context context) {
        return new PrefStorageImpl(context);
    }

}
