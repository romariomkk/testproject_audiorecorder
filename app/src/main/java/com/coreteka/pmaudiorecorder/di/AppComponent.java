package com.coreteka.pmaudiorecorder.di;

import android.app.Application;

import com.coreteka.pmaudiorecorder.PMAudioRecorderApp;
import com.coreteka.pmaudiorecorder.di.modules.ActivityModule;
import com.coreteka.pmaudiorecorder.di.modules.AppModule;
import com.coreteka.pmaudiorecorder.di.modules.PresenterModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(modules = {ActivityModule.class, PresenterModule.class, AppModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

    void inject(PMAudioRecorderApp app);
}
