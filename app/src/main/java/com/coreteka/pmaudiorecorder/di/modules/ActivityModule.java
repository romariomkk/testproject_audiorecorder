package com.coreteka.pmaudiorecorder.di.modules;

import com.coreteka.pmaudiorecorder.view.screens.audio.RecordActivity;
import com.coreteka.pmaudiorecorder.view.screens.login.LoginActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract LoginActivity contributeLoginActivity();

    @ContributesAndroidInjector
    abstract RecordActivity contributeRecordActivity();
}
