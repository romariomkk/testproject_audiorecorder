package com.coreteka.pmaudiorecorder.di.modules;

import com.coreteka.pmaudiorecorder.api.PMApi;
import com.coreteka.pmaudiorecorder.core.audio.AudioManager;
import com.coreteka.pmaudiorecorder.core.db.FileDao;
import com.coreteka.pmaudiorecorder.core.repo.AudioRepo;
import com.coreteka.pmaudiorecorder.core.repo.AuthRepository;
import com.coreteka.pmaudiorecorder.core.repo.GeneralAppRepository;
import com.coreteka.pmaudiorecorder.core.repo.impl.AudioRepoImpl;
import com.coreteka.pmaudiorecorder.core.repo.impl.AuthRepositoryImpl;
import com.coreteka.pmaudiorecorder.core.repo.impl.GeneralAppRepositoryImpl;
import com.coreteka.pmaudiorecorder.core.storage.AuthStorage;
import com.coreteka.pmaudiorecorder.core.storage.GeneralDataStorage;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = ManagerModule.class)
public class RepoModule {

    @Provides
    @Singleton
    AuthRepository provideAuthRepository(PMApi api,
                                         AuthStorage authStorage) {
        return new AuthRepositoryImpl(api, authStorage);
    }

    @Provides
    @Singleton
    AudioRepo provideAudioRepo(AudioManager audioManager, PMApi api, FileDao fileDao,
                               GeneralDataStorage generalDataStorage) {
        return new AudioRepoImpl(audioManager, api, fileDao, generalDataStorage);
    }

    @Provides
    @Singleton
    GeneralAppRepository provideGeneralAppRepository(GeneralDataStorage generalDataStorage) {
        return new GeneralAppRepositoryImpl(generalDataStorage);
    }

}
