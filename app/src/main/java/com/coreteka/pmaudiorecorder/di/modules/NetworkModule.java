package com.coreteka.pmaudiorecorder.di.modules;

import android.util.Log;

import com.coreteka.pmaudiorecorder.BuildConfig;
import com.coreteka.pmaudiorecorder.api.PMApi;
import com.coreteka.pmaudiorecorder.api.TokenInterceptor;
import com.coreteka.pmaudiorecorder.core.storage.AuthStorage;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

@Module
public class NetworkModule {

    @Singleton
    @Provides
    Gson provideGson() {
        return new GsonBuilder()
                .create();
    }

    @Singleton
    @Provides
    HttpLoggingInterceptor providesInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(message -> {
            if (Log.isLoggable("OkHttp", Log.DEBUG)) {
                Timber.tag("OkHttp").d(message);
            } else {
                Timber.tag("OkHttp").i(message);
            }
        });
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

    @Singleton
    @Provides
    TokenInterceptor providesTokenInterceptor(AuthStorage authStorage) {
        return new TokenInterceptor(authStorage);
    }

    @Singleton
    @Provides
    OkHttpClient providesOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor,
                                      TokenInterceptor tokenInterceptor) {
        return new OkHttpClient.Builder()
                .addNetworkInterceptor(httpLoggingInterceptor)
                .addNetworkInterceptor(new StethoInterceptor())
                .addInterceptor(tokenInterceptor)
                .readTimeout(60L, TimeUnit.SECONDS)
                .connectTimeout(60L, TimeUnit.SECONDS)
                .writeTimeout(60L, TimeUnit.SECONDS)
                .build();
    }

    @Singleton
    @Provides
    PMApi providesRetrofit(OkHttpClient client, Gson gson) {
        PMApi api = new Retrofit.Builder()
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build()
                .create(PMApi.class);
        return api;
    }

}
