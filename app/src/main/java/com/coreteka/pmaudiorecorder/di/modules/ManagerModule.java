package com.coreteka.pmaudiorecorder.di.modules;

import android.content.Context;

import com.coreteka.pmaudiorecorder.core.audio.AudioManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ManagerModule {

    @Provides
    @Singleton
    AudioManager provideAudioManager(Context context) {
        return new AudioManager(context);
    }

}
