package com.coreteka.pmaudiorecorder.core.repo.impl;

import com.coreteka.pmaudiorecorder.core.repo.GeneralAppRepository;
import com.coreteka.pmaudiorecorder.core.storage.GeneralDataStorage;

import io.reactivex.Completable;
import io.reactivex.Single;


public class GeneralAppRepositoryImpl implements GeneralAppRepository {

    private GeneralDataStorage generalDataStorage;

    public GeneralAppRepositoryImpl(GeneralDataStorage generalDataStorage) {
        this.generalDataStorage = generalDataStorage;
    }

    @Override
    public Single<Integer> totalRecordings() {
        return generalDataStorage.totalRecordings();
    }

    @Override
    public Completable resetRecordingsNumber() {
        return generalDataStorage.resetTotalRecordings();
    }
}
