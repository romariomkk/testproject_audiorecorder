package com.coreteka.pmaudiorecorder.core.repo;

import io.reactivex.Completable;
import io.reactivex.Single;


public interface GeneralAppRepository {

    Completable resetRecordingsNumber();

    Single<Integer> totalRecordings();

}
