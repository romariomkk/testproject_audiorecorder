package com.coreteka.pmaudiorecorder.core.interactors;

import android.support.annotation.NonNull;

import com.coreteka.pmaudiorecorder.core.repo.AuthRepository;
import com.coreteka.pmaudiorecorder.core.repo.GeneralAppRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class AuthInteractor {

    private final AuthRepository authRepository;
    private final GeneralAppRepository generalAppRepository;

    @Inject
    public AuthInteractor(AuthRepository authRepository, GeneralAppRepository generalAppRepository) {
        this.authRepository = authRepository;
        this.generalAppRepository = generalAppRepository;
    }

    public Single<String> email() {
        return authRepository.email()
                .subscribeOn(Schedulers.io());
    }

    public Completable login(@NonNull String login, @NonNull String password) {
        return authRepository.login(login, password)
                .subscribeOn(Schedulers.io());
    }

    public Completable isAuthenticated() {
        return authRepository.hasToken()
                .subscribeOn(Schedulers.io());
    }

    public Completable logout() {
        return authRepository.logout()
                .subscribeOn(Schedulers.io())
                .andThen(generalAppRepository.resetRecordingsNumber());
    }
}
