package com.coreteka.pmaudiorecorder.core.storage;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface AuthStorage {

    Completable token(String token);

    String rawToken();

    Completable hasToken();

    Single<String> token();

    Completable deleteToken();

    Completable email(String login);

    Single<String> email();

    Completable deleteEmail();
}
