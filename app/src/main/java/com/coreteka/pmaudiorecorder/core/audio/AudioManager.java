package com.coreteka.pmaudiorecorder.core.audio;

import android.content.Context;
import android.media.AudioFormat;
import android.media.MediaRecorder;

import com.coreteka.pmaudiorecorder.util.FileUtil;
import com.coreteka.pmaudiorecorder.util.Keys;

import java.io.File;

import cafe.adriel.androidaudioconverter.AndroidAudioConverter;
import cafe.adriel.androidaudioconverter.callback.IConvertCallback;
import io.reactivex.Observable;
import omrecorder.AudioRecordConfig;
import omrecorder.OmRecorder;
import omrecorder.PullTransport;
import omrecorder.PullableSource;
import omrecorder.Recorder;
import timber.log.Timber;

public class AudioManager {

    private Recorder recorder;
    private final Context context;

    private String fileName;

    public AudioManager(Context context) {
        this.context = context;
    }

    public Observable<Object> startRecording() {

        return Observable.create(emitter -> {

            fileName = FileUtil.generateFileName(context, Keys.AudioFormat.WAV);
            Timber.e(fileName);
            recorder = OmRecorder.wav(
                    new PullTransport.Default(new PullableSource.Default(
                            new AudioRecordConfig.Default(
                                    MediaRecorder.AudioSource.MIC,
                                    AudioFormat.ENCODING_PCM_16BIT,
                                    AudioFormat.CHANNEL_IN_MONO,
                                    Keys.AudioSampleRate.HZ_32000.getSampleRate()))),
                    new File(fileName));

            recorder.startRecording();

            emitter.onNext(fileName);
        });
    }

    public Observable<String> stopRecording() {
        return Observable.create(emitter -> {
            if (recorder != null) {
                recorder.stopRecording();
                recorder = null;
            }
            emitter.onNext(fileName);
        });
    }

    public Observable<File> convert(String fileName) {
        return Observable.create(emitter -> {
            File nonFlacFile = new File(fileName);
            IConvertCallback callback = new IConvertCallback() {
                @Override
                public void onSuccess(File convertedFile) {
                    Timber.d("Conversion successful into: %s", convertedFile.getAbsolutePath());
                    emitter.onNext(convertedFile);
                }
                @Override
                public void onFailure(Exception error) {
                    emitter.onError(error);
                }
            };

            AndroidAudioConverter.with(context)
                    .setFile(nonFlacFile)
                    .setFormat(cafe.adriel.androidaudioconverter.model.AudioFormat.FLAC)
                    .setCallback(callback)
                    .convert();
        });
    }
}
