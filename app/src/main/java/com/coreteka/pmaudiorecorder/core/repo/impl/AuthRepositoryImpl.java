package com.coreteka.pmaudiorecorder.core.repo.impl;

import android.support.annotation.NonNull;

import com.coreteka.pmaudiorecorder.api.PMApi;
import com.coreteka.pmaudiorecorder.api.request.AuthRequest;
import com.coreteka.pmaudiorecorder.api.response.TokenResponse;
import com.coreteka.pmaudiorecorder.core.repo.AuthRepository;
import com.coreteka.pmaudiorecorder.core.storage.AuthStorage;

import io.reactivex.Completable;
import io.reactivex.Single;


public class AuthRepositoryImpl implements AuthRepository {

    private final PMApi api;
    private final AuthStorage authStorage;

    public AuthRepositoryImpl(PMApi api, AuthStorage authStorage) {
        this.api = api;
        this.authStorage = authStorage;
    }

    @Override
    public Completable login(@NonNull String login, @NonNull String password) {
        return api.login(new AuthRequest(login, password))
                .map(TokenResponse::getToken)
                .flatMapCompletable(authStorage::token)
                .andThen(authStorage.email(login));
    }

    @Override
    public Completable hasToken() {
        return authStorage.hasToken();
    }

    @Override
    public Completable logout() {
        return authStorage.deleteToken();
    }

    @Override
    public Single<String> email() {
        return authStorage.email();
    }
}
