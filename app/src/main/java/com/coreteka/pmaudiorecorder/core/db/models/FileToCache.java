package com.coreteka.pmaudiorecorder.core.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.coreteka.pmaudiorecorder.core.db.FileDao;

@Entity(tableName = FileDao.FILE_TABLE)
public class FileToCache {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String fileUri;

    public FileToCache(String fileUri) {
        this.fileUri = fileUri;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileUri() {
        return fileUri;
    }

    public void setFileUri(String fileUri) {
        this.fileUri = fileUri;
    }
}
