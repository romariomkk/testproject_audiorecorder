package com.coreteka.pmaudiorecorder.core.repo.impl;

import com.coreteka.pmaudiorecorder.api.PMApi;
import com.coreteka.pmaudiorecorder.core.audio.AudioManager;
import com.coreteka.pmaudiorecorder.core.db.FileDao;
import com.coreteka.pmaudiorecorder.core.db.models.FileToCache;
import com.coreteka.pmaudiorecorder.core.repo.AudioRepo;
import com.coreteka.pmaudiorecorder.core.storage.GeneralDataStorage;
import com.coreteka.pmaudiorecorder.util.FileUtil;

import java.io.File;
import java.io.FileNotFoundException;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import timber.log.Timber;

public class AudioRepoImpl implements AudioRepo {

    private final AudioManager audioManager;
    private final PMApi api;
    private final FileDao fileDao;
    private final GeneralDataStorage generalDataStorage;


    public AudioRepoImpl(AudioManager audioManager,
                         PMApi api,
                         FileDao fileDao,
                         GeneralDataStorage generalDataStorage) {
        this.audioManager = audioManager;
        this.api = api;
        this.fileDao = fileDao;
        this.generalDataStorage = generalDataStorage;
    }

    @Override
    public Observable<Object> startRecording() {
        return audioManager.startRecording();
    }

    @Override
    public Completable forceStopRecording() {
        return audioManager.stopRecording()
                .flatMapCompletable(fileName -> Completable.fromAction(() -> FileUtil.delete(fileName)));
    }

    @Override
    public Observable<Integer> stopRecording() {
        return audioManager.stopRecording()
                .flatMap(fileName -> audioManager.convert(fileName)
                        .doOnNext(convertedFile -> FileUtil.delete(fileName)))
                .observeOn(Schedulers.io())
                .flatMapSingle(file ->
                        sendAudio(file)
                                .doOnComplete(file::delete)
                                .doOnError(throwable -> {
                                    Timber.e("File parsing error. Saved to DB");
                                    Completable.fromAction(() -> fileDao.insert(new FileToCache(file.getAbsolutePath()))).subscribe();
                                })
                                .andThen(generalDataStorage.addSingleRecordingDone()));
    }

    @Override
    public Completable syncAllUnsynced() {
        return fileDao.selectAll()
                .flatMapIterable(fileToCaches -> fileToCaches)
                .flatMapCompletable(
                        file -> sendAudio(new File(file.getFileUri()))
                                .doOnError(t -> {
                                    if (t instanceof FileNotFoundException) {
                                        Timber.e(t, "File not found in cache");
                                        fileDao.deleteWhereFileNameIs(file.getFileUri());
                                    }
                                })
                                .doOnComplete(() -> {
                                    fileDao.deleteWhereFileNameIs(file.getFileUri());
                                    FileUtil.delete(file.getFileUri());

                                    generalDataStorage.addSingleRecordingDone();
                                }));
    }

    public Completable sendAudio(File file) {
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("multipart/form-data; boundary=boundary; charset=utf-8"),
                        file
                );

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        return api.sendAudio(body);
    }
}
