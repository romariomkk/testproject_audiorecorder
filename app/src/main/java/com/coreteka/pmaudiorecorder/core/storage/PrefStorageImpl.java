package com.coreteka.pmaudiorecorder.core.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import io.reactivex.Completable;
import io.reactivex.Single;

public class PrefStorageImpl implements AuthStorage, GeneralDataStorage {

    private static final String STORAGE = "pmaudio-private-storage";
    private static final String TOKEN = "token";
    private static final String EMAIL = "email";
    private static final String RECORDINGS_COUNTER = "counter";

    private SharedPreferences prefs;

    public PrefStorageImpl(Context context) {
        prefs = context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
    }

    @Override
    public Completable token(String token) {
        return Completable.fromAction(() -> prefs.edit().putString(TOKEN, token).apply());
    }

    @Override
    public String rawToken() {
        return prefs.getString(TOKEN, "");
    }

    @Override
    public Completable hasToken() {
        return TextUtils.isEmpty(rawToken()) ? Completable.error(new Throwable()) : Completable.complete();
    }

    @Override
    public Single<String> token() {
        return Single.just(rawToken());
    }

    @Override
    public Completable deleteToken() {
        return Completable.fromAction(() -> prefs.edit().remove(TOKEN).apply());
    }

    @Override
    public Completable email(String login) {
        return Completable.fromAction(() -> prefs.edit().putString(EMAIL, login).apply());
    }

    @Override
    public Single<String> email() {
        return Single.just(prefs.getString(EMAIL, ""));
    }

    @Override
    public Completable deleteEmail() {
        return Completable.fromAction(() -> prefs.edit().remove(EMAIL).apply());
    }


    @Override
    public Single<Integer> totalRecordings() {
        return Single.just(prefs.getInt(RECORDINGS_COUNTER, 0));
    }

    @Override
    public Single<Integer> addSingleRecordingDone() {
        return totalRecordings()
                .flatMap(count -> {
                    prefs.edit().putInt(RECORDINGS_COUNTER, count + 1).commit();
                    return totalRecordings();
                });
//                .flatMapCompletable(count ->
//                        Completable.fromAction(() ->
//                                ))
//                .andThen(totalRecordings());
    }

    @Override
    public Completable resetTotalRecordings() {
        return Completable.fromAction(() -> prefs.edit().putInt(RECORDINGS_COUNTER, 0).commit());
    }
}
