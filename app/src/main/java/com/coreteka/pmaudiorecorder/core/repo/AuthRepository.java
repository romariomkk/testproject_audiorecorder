package com.coreteka.pmaudiorecorder.core.repo;

import android.support.annotation.NonNull;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface AuthRepository {

    Completable login(@NonNull String login, @NonNull String password);

    Completable hasToken();

    Completable logout();

    Single<String> email();
}
