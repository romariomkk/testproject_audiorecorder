package com.coreteka.pmaudiorecorder.core.storage;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface GeneralDataStorage {

    Single<Integer> totalRecordings();

    Single<Integer> addSingleRecordingDone();

    Completable resetTotalRecordings();

}
