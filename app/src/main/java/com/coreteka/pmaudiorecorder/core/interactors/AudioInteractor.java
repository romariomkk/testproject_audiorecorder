package com.coreteka.pmaudiorecorder.core.interactors;

import com.coreteka.pmaudiorecorder.core.repo.AudioRepo;
import com.coreteka.pmaudiorecorder.core.repo.GeneralAppRepository;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class AudioInteractor {

    private final AudioRepo audioRepo;
    private final GeneralAppRepository generalAppRepository;

    @Inject
    public AudioInteractor(AudioRepo audioRepo, GeneralAppRepository generalAppRepository) {
        this.audioRepo = audioRepo;
        this.generalAppRepository = generalAppRepository;
    }

    public Observable<Object> startRecording() {
        return audioRepo.startRecording();
    }

    public Completable forceStopRecording() {
        return audioRepo.forceStopRecording()
                .subscribeOn(Schedulers.io());
    }

    public Observable<Integer> stopRecording() {
        return audioRepo.stopRecording()
                .subscribeOn(Schedulers.io());
    }

    public Completable syncAllUnsynced() {
        return audioRepo.syncAllUnsynced()
                .subscribeOn(Schedulers.io());
    }


    public Single<Integer> totalRecordings() {
        return generalAppRepository.totalRecordings()
                .subscribeOn(Schedulers.io());
    }

}
