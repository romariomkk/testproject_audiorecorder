package com.coreteka.pmaudiorecorder.core.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.coreteka.pmaudiorecorder.core.db.models.FileToCache;

@Database(entities = {FileToCache.class}, version = 1, exportSchema = false)
public abstract class PMAudioDatabase extends RoomDatabase {

    public abstract FileDao getFileDao();

}
