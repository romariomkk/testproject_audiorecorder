package com.coreteka.pmaudiorecorder.core.repo;

import io.reactivex.Completable;
import io.reactivex.Observable;

public interface AudioRepo {

    Observable<Object> startRecording();

    Completable forceStopRecording();

    Observable<Integer> stopRecording();

    Completable syncAllUnsynced();

}
