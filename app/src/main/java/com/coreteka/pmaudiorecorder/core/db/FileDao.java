package com.coreteka.pmaudiorecorder.core.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.coreteka.pmaudiorecorder.core.db.models.FileToCache;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class FileDao {

    public static final String FILE_TABLE = "fileTable";

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(FileToCache fileToCache);

    @Query("SELECT * FROM " + FILE_TABLE)
    public abstract Flowable<List<FileToCache>> selectAll();

    @Query("DELETE FROM " + FILE_TABLE + " WHERE fileUri = :fileName")
    public abstract void deleteWhereFileNameIs(String fileName);

}
