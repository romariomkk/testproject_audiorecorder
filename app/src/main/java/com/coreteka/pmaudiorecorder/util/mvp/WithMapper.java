package com.coreteka.pmaudiorecorder.util.mvp;

import android.support.annotation.Nullable;

public interface WithMapper<From, With, To> {
    default To map(From from) {
        return map(from, null);
    }

    To map(From from, @Nullable With with);
}
