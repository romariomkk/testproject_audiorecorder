package com.coreteka.pmaudiorecorder.util;

import android.support.annotation.LayoutRes;
import android.support.annotation.MenuRes;

import com.coreteka.pmaudiorecorder.util.layout.annotation.RequiresMenu;
import com.coreteka.pmaudiorecorder.util.layout.annotation.RequiresView;
import com.coreteka.pmaudiorecorder.util.mvp.BasePresenter;
import com.coreteka.pmaudiorecorder.util.mvp.annotation.RequiresPresenter;

public class AnnotationUtil {

    public static Class<? extends BasePresenter> findPresenterClass(Object object) {
        return ((RequiresPresenter) object.getClass().getAnnotation(RequiresPresenter.class)).value();
    }

    public static @LayoutRes
    int findViewId(Object object) {
        if (object.getClass().isAnnotationPresent(RequiresView.class))
            return object.getClass().getAnnotation(RequiresView.class).value();
        else
            return -1;
    }

    public static @MenuRes
    int findMenuId(Object object) {
        if (object.getClass().isAnnotationPresent(RequiresMenu.class))
            return object.getClass().getAnnotation(RequiresMenu.class).value();
        else
            return -1;
    }

    public static boolean hasPresenter(Object object) {
        return object.getClass().isAnnotationPresent(RequiresPresenter.class);
    }

}
