package com.coreteka.pmaudiorecorder.util.mvp.viewextention;


public interface UiView {

    void enableTouch();

    void disableTouch();

}
