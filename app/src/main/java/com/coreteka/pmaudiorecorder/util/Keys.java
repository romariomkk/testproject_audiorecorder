package com.coreteka.pmaudiorecorder.util;

public class Keys {

    public static final int VALUE_TO_START_COUNTDOWN_FROM = 29;

    public enum AudioSampleRate {
        HZ_48000,
        HZ_44100,
        HZ_32000,
        HZ_22050,
        HZ_16000,
        HZ_11025,
        HZ_8000;

        public int getSampleRate(){
            return Integer.parseInt(name().replace("HZ_", ""));
        }
    }

    public enum AudioFormat {
        FLAC,
        WAV;

        public String getFormatName() {
            return name().toLowerCase();
        }
    }

    public static class ErrorKeys {
        public static String BAD_CREDS_CODE = "badCredentials";
    }

}
