package com.coreteka.pmaudiorecorder.util.mvp;

public interface BasePresenter<V extends BaseView> {

    void bindView(V view);

    void unbindView();

    void onCreate(V view);

    void onCleanup();

    boolean isBinded();
}
