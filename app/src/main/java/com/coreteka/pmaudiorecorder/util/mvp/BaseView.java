package com.coreteka.pmaudiorecorder.util.mvp;


import com.coreteka.pmaudiorecorder.util.mvp.viewextention.ErrorView;
import com.coreteka.pmaudiorecorder.util.mvp.viewextention.MessageView;
import com.coreteka.pmaudiorecorder.util.mvp.viewextention.UiView;

public interface BaseView extends ErrorView, MessageView, UiView {

}
