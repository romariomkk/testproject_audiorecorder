package com.coreteka.pmaudiorecorder.util.rx;

import android.arch.lifecycle.LifecycleObserver;

import com.coreteka.pmaudiorecorder.util.mvp.BaseView;

import java.lang.ref.WeakReference;

import io.reactivex.observers.DisposableMaybeObserver;
import timber.log.Timber;

public abstract class CallbackMaybeWrapper<T> extends DisposableMaybeObserver<T>
        implements LifecycleObserver {

    private WeakReference<BaseView> viewWR;

    public CallbackMaybeWrapper(BaseView view) {
        this.viewWR = new WeakReference<>(view);
    }

    @Override
    public void onSuccess(T t) {
        success(t);
    }

    @Override
    public void onError(Throwable e) {
        Timber.e(e);
        ErrorHandler.handle(e, viewWR.get());
    }

    @Override
    public void onComplete() {
        complete();
    }

    public abstract void success(T t);

    public abstract void complete();

}
