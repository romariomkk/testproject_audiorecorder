package com.coreteka.pmaudiorecorder.util;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateUtil {

    public enum Type {
        EMAIL("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        
        public final Pattern pattern;

        Type(Pattern pattern) {
            this.pattern = pattern;
        }

        Type(String regex) {
            this.pattern = Pattern.compile(regex);
        }

        Type(String regex, int flag) {
            this.pattern = Pattern.compile(regex, flag);
        }
    }


    public static boolean validate(String s, Type type) {
        return validate(s, type, false);
    }

    public static boolean validate(String s, Type type, boolean canBeEmpty) {
        if (TextUtils.isEmpty(s))
            return canBeEmpty;
        Matcher matcher = type.pattern.matcher(s);
        return matcher.find();
    }
}
