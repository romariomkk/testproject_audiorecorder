package com.coreteka.pmaudiorecorder.util.rx;

import com.coreteka.pmaudiorecorder.R;
import com.coreteka.pmaudiorecorder.util.mvp.viewextention.ErrorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;

import okhttp3.ResponseBody;
import retrofit2.HttpException;

import static com.coreteka.pmaudiorecorder.util.Keys.ErrorKeys.BAD_CREDS_CODE;

@SuppressWarnings("WeakerAccess")
public class ErrorHandler {

    public static void handle(Throwable e, ErrorView view) {
        if (view == null)
            return;
        if (isHttpException(e)) {
            if (isSessionTimeout((HttpException) e)) {
                view.showSessionTimeout();
            } else {
                ResponseBody responseBody = ((HttpException) e).response().errorBody();
                if (getErrorCode(responseBody).equals(BAD_CREDS_CODE)) {
                    view.showError(R.string.error_bad_credentials);
                }
                //view.showError(getErrorMessage(responseBody));
            }
        } else if (isTimeOut(e)) {
            view.showTimeOutError();
        } else if (isNetwork(e) || isConnectException(e)) {
            view.showNetworkError();
        }
    }

    private static String getErrorCode(ResponseBody responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody.string());
            return jsonObject.getString("errorCode");
        } catch (JSONException | IOException e) {
            return e.getMessage();
        }
    }

    private static String getErrorMessage(ResponseBody responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody.string());
            return jsonObject.getString("errorCode");
        } catch (JSONException | IOException e) {
            return e.getMessage();
        }
    }

    public static boolean isNetwork(Throwable throwable) {
        return throwable instanceof IOException;
    }

    public static boolean isTimeOut(Throwable throwable) {
        return throwable instanceof SocketTimeoutException;
    }

    public static boolean isHttpException(Throwable throwable) {
        return throwable instanceof HttpException;
    }

    public static boolean isConnectException(Throwable throwable) {
        return throwable instanceof ConnectException;
    }

    public static boolean isSessionTimeout(HttpException throwable) {
        return throwable.code() == 401;
    }

}
