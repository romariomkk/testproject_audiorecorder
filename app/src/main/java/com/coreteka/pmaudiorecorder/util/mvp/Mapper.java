package com.coreteka.pmaudiorecorder.util.mvp;

public interface Mapper<From, To> {

    To map(From from);

}
