package com.coreteka.pmaudiorecorder.util.rx;

import android.arch.lifecycle.LifecycleObserver;

import com.coreteka.pmaudiorecorder.util.mvp.BaseView;

import java.lang.ref.WeakReference;

import io.reactivex.observers.DisposableObserver;
import timber.log.Timber;

public abstract class CallbackObserverWrapper<T> extends DisposableObserver<T>
        implements LifecycleObserver {

    private WeakReference<BaseView> viewWR;

    public CallbackObserverWrapper(BaseView view) {
        this.viewWR = new WeakReference<>(view);
    }

    protected abstract void success(T t);

    @Override
    public void onNext(T t) {
        success(t);
    }

    @Override
    public void onError(Throwable e) {
        Timber.e(e);
        ErrorHandler.handle(e, viewWR.get());
    }

    @Override
    public void onComplete() {
    }


}
