package com.coreteka.pmaudiorecorder.util.mvp;

public interface LoadingView extends BaseView {
    
    void onStartLoading();
    
    void onFinishedLoading();
    
}
