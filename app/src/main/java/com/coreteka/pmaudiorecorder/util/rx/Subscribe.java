package com.coreteka.pmaudiorecorder.util.rx;

import com.coreteka.pmaudiorecorder.util.mvp.BaseView;

import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

public class Subscribe {
    public static CallbackCompletableWrapper completable(BaseView view,
                                                         Action action) {
        return completable(view, action, null);
    }

    public static CallbackCompletableWrapper completableWithError(BaseView view,
                                                                  Action action,
                                                                  Consumer<Throwable> throwable) {
        return new CallbackCompletableWrapper(view) {
            @Override
            public void complete() {
                try {
                    action.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                if (throwable != null) {
                    try {
                        throwable.accept(e);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }
        };
    }

    public static CallbackCompletableWrapper completable(BaseView view,
                                                         Action action,
                                                         Consumer<Throwable> throwable) {
        return new CallbackCompletableWrapper(view) {
            @Override
            public void complete() {
                try {
                    action.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable e) {
                if (throwable != null) {
                    try {
                        throwable.accept(e);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                } else {
                    super.onError(e);
                }
            }
        };
    }

    public static <T> CallbackMaybeWrapper<T> maybe(BaseView view,
                                                    Consumer<T> success,
                                                    Action action) {
        return maybe(view, success, action, null);
    }

    public static <T> CallbackMaybeWrapper<T> maybe(BaseView view,
                                                    Consumer<T> success,
                                                    Consumer<Throwable> throwable) {
        return maybe(view, success, null, throwable);
    }

    public static <T> CallbackMaybeWrapper<T> maybe(BaseView view,
                                                    Consumer<T> success) {
        return maybe(view, success, null, null);
    }

    public static <T> CallbackMaybeWrapper<T> maybe(BaseView view,
                                                    Consumer<T> success,
                                                    Action action,
                                                    Consumer<Throwable> throwable) {
        return new CallbackMaybeWrapper<T>(view) {
            @Override
            public void success(T t) {
                try {
                    success.accept(t);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void complete() {
                if (action != null) {
                    try {
                        action.run();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                if (throwable != null) {
                    try {
                        throwable.accept(e);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                } else {
                    super.onError(e);
                }
            }
        };
    }

    public static <T> CallbackMaybeWrapper<T> maybeWithError(BaseView view,
                                                             Consumer<T> success,
                                                             Action action,
                                                             Consumer<Throwable> throwable) {
        return new CallbackMaybeWrapper<T>(view) {
            @Override
            public void success(T t) {
                try {
                    success.accept(t);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void complete() {
                if (action != null) {
                    try {
                        action.run();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                if (throwable != null) {
                    try {
                        throwable.accept(e);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }
        };
    }

    public static <T> CallbackObserverWrapper<T> observer(BaseView view,
                                                          Consumer<T> success) {
        return observer(view, success, null);
    }

    public static <T> CallbackObserverWrapper<T> observer(BaseView view,
                                                          Consumer<T> success,
                                                          Consumer<Throwable> throwable) {
        return new CallbackObserverWrapper<T>(view) {
            @Override
            protected void success(T t) {
                try {
                    success.accept(t);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable e) {
                if (throwable != null) {
                    try {
                        throwable.accept(e);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                } else {
                    super.onError(e);
                }
            }
        };
    }

    public static <T> CallbackObserverWrapper<T> observerWithError(BaseView view,
                                                                   Consumer<T> success,
                                                                   Consumer<Throwable> throwable) {
        return new CallbackObserverWrapper<T>(view) {
            @Override
            protected void success(T t) {
                try {
                    success.accept(t);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                if (throwable != null) {
                    try {
                        throwable.accept(e);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }
        };
    }

    public static <T> CallbackSingleWrapper<T> single(BaseView view,
                                                      Consumer<T> success) {
        return single(view, success, null);
    }

    public static <T> CallbackSingleWrapper<T> single(BaseView view,
                                                      Consumer<T> success,
                                                      Consumer<Throwable> throwable) {
        return new CallbackSingleWrapper<T>(view) {
            @Override
            protected void success(T t) {
                try {
                    success.accept(t);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable e) {
                if (throwable != null) {
                    try {
                        throwable.accept(e);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                } else {
                    super.onError(e);
                }
            }
        };
    }

    public static <T> CallbackSingleWrapper<T> singleWithError(BaseView view,
                                                               Consumer<T> success,
                                                               Consumer<Throwable> throwable) {
        return new CallbackSingleWrapper<T>(view) {
            @Override
            protected void success(T t) {
                try {
                    success.accept(t);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                if (throwable != null) {
                    try {
                        throwable.accept(e);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }
        };
    }
}
