package com.coreteka.pmaudiorecorder.util.rx;


import android.arch.lifecycle.LifecycleObserver;

import com.coreteka.pmaudiorecorder.util.mvp.BaseView;

import java.lang.ref.WeakReference;

import io.reactivex.observers.DisposableSingleObserver;
import timber.log.Timber;

public abstract class CallbackSingleWrapper<T> extends DisposableSingleObserver<T> implements LifecycleObserver {

    private WeakReference<BaseView> viewWR;

    public CallbackSingleWrapper(BaseView view) {
        this.viewWR = new WeakReference<>(view);
    }

    @Override
    public void onSuccess(T t) {
        success(t);
    }

    protected abstract void success(T t);

    @Override
    public void onError(Throwable e) {
        Timber.e(e);
        ErrorHandler.handle(e, viewWR.get());
    }
}
