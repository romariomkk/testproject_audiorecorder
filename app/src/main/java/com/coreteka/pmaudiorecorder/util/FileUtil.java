package com.coreteka.pmaudiorecorder.util;

import android.content.Context;

import java.io.File;

public class FileUtil {

    private static String AUDIO_FILES_CACHE_FOLDER = "audio";


    private static String getRoot(Context context) {
        return context.getExternalFilesDir(null).getAbsolutePath();
    }

    private static String getCache(Context context) {
        return context.getCacheDir().getAbsolutePath();
    }

    private static File getAudioCacheFolder(Context context) {
        String path = new StringBuilder(getCache(context))
                .append("/")
                .append(AUDIO_FILES_CACHE_FOLDER)
                .toString();
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static String getFilePath(Context context, String fileName) {
        return getAudioCacheFolder(context) + "/" + fileName;
    }

    public static String generateFileName(Context context, Keys.AudioFormat audioFormat) {
        return getAudioCacheFolder(context) + "/" + System.currentTimeMillis() + "." + audioFormat.getFormatName();
    }

    public static void delete(String fileUri) {
        File file = new File(fileUri);
        file.delete();
    }
}
