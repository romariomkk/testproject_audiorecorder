package com.coreteka.pmaudiorecorder.util;

public class FormatUtil {

    /**
     * Hours : min : sec
     */
    public static String formatTime(int milliseconds) {
        return getTwoDecimalsValue(milliseconds / 3600000) + ":"
                + getTwoDecimalsValue(milliseconds / 60000) + ":"
                + getTwoDecimalsValue((Keys.VALUE_TO_START_COUNTDOWN_FROM - 1) - ((milliseconds % 60000) / 1000)) + "."
                + (9 - (milliseconds % 1000) / 100);
    }

    private static String getTwoDecimalsValue(int value) {
        if (value >= 0 && value <= 9) {
            return "0" + value;
        } else {
            return value + "";
        }
    }

}
