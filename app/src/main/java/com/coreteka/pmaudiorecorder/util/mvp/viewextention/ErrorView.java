package com.coreteka.pmaudiorecorder.util.mvp.viewextention;


import android.support.annotation.Nullable;
import android.support.annotation.StringRes;


public interface ErrorView {

    default void showError(String string) {
        showError(string, null);
    }

    void showError(@StringRes int strRes);
    
    void showError(String string, @Nullable Runnable runnable);
    
    void showError(@StringRes int strRes, @Nullable Runnable runnable);

    void showNetworkError();

    void showTimeOutError();

    void showSessionTimeout();

}
