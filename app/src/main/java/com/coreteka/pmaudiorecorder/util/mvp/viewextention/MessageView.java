package com.coreteka.pmaudiorecorder.util.mvp.viewextention;

import android.support.annotation.StringRes;

public interface MessageView {

    void showToast(@StringRes int id);

    void showToast(String message);

    void showDialog(String title, String message, Runnable... runnables);

    void showDialog(@StringRes int title, String message, Runnable... runnables);

    void showDialog(@StringRes int title, @StringRes int message, Runnable... runnables);

}
