package com.coreteka.pmaudiorecorder.util;

import android.os.Handler;
import android.os.Looper;
import android.view.View;

public class UIUtil {

    public static void onClick(View view, View.OnClickListener listener) {
        Handler handler = view.getHandler() != null
                ? view.getHandler()
                : new Handler(Looper.getMainLooper());
        view.setOnClickListener(v -> {
            handler.removeCallbacks(null);
            handler.post(() -> listener.onClick(v));
        });
    }

}
