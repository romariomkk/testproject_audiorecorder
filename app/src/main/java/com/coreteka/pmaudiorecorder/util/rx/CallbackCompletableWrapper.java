package com.coreteka.pmaudiorecorder.util.rx;

import android.arch.lifecycle.LifecycleObserver;

import com.coreteka.pmaudiorecorder.util.mvp.BaseView;

import java.lang.ref.WeakReference;

import io.reactivex.observers.DisposableCompletableObserver;
import timber.log.Timber;

public abstract class CallbackCompletableWrapper extends DisposableCompletableObserver
        implements LifecycleObserver {

    private WeakReference<BaseView> viewWR;

    public CallbackCompletableWrapper(BaseView view) {
        this.viewWR = new WeakReference<>(view);
    }

    @Override
    public void onComplete() {
        complete();
    }

    public abstract void complete();

    @Override
    public void onError(Throwable e) {
        Timber.e(e);
        ErrorHandler.handle(e, viewWR.get());
    }
}
