package com.coreteka.pmaudiorecorder.view.base;

import com.coreteka.pmaudiorecorder.util.mvp.BasePresenter;
import com.coreteka.pmaudiorecorder.util.mvp.BaseView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;

public abstract class AbsPresenter<V extends BaseView> implements BasePresenter<V> {

    public V view;
    private List<Disposable> disposables = new ArrayList<>();
    private List<Disposable> subjectDisposables = new ArrayList<>();

    @Override
    public void bindView(V view) {
        this.view = view;
    }

    @Override
    public void onCreate(V view) {
        this.view = view;
    }

    @Override
    public void onCleanup() {
        dispose();
        disposeSubjects();
    }

    @Override
    public void unbindView() {
        this.view = null;
        disposeSubjects();
    }

    @Override
    public boolean isBinded() {
        return view != null;
    }

    public void add(Disposable disposable) {
        disposables.add(disposable);
    }

    protected void addSubject(Disposable disposable) {
        subjectDisposables.add(disposable);
    }

    protected void disposeSubjects() {
        for (Disposable disposable : subjectDisposables) {
            disposable.dispose();
        }
        subjectDisposables.clear();
    }

    protected void dispose() {
        for (Disposable disposable : disposables) {
            disposable.dispose();
        }
        disposables.clear();
    }
}
