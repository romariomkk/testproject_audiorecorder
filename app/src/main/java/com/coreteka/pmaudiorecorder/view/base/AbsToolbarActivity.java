package com.coreteka.pmaudiorecorder.view.base;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.FrameLayout;

import com.coreteka.pmaudiorecorder.R;
import com.coreteka.pmaudiorecorder.util.AnnotationUtil;

import butterknife.BindView;

/**
 * Represents activity with toolbar
 *
 * @param <P> Presenter
 */
public abstract class AbsToolbarActivity<P extends AbsPresenter> extends AbsActivity<P> {

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

    }

    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void setTitle(@StringRes int id) {
        getSupportActionBar().setTitle(id);
    }

    public void setSubtitle(String title) {
        getSupportActionBar().setSubtitle(title);
    }

    public void setSubtitle(@StringRes int id) {
        getSupportActionBar().setSubtitle(id);
    }

    public void setNavigation(@DrawableRes int id, View.OnClickListener clickListener) {
        toolbar.setNavigationIcon(id);
        toolbar.setNavigationOnClickListener(clickListener);
    }

    public void setNavigation(@DrawableRes int id) {
        toolbar.setNavigationIcon(id);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        int menuId = AnnotationUtil.findMenuId(this);
        if (menuId != -1)
            getMenuInflater().inflate(menuId, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void translucentStatusBar() {
        super.translucentStatusBar();
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) toolbar.getLayoutParams();
        params.setMargins(0, getStatusBarHeight(), 0, 0);
        toolbar.setLayoutParams(params);
    }

    int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}
