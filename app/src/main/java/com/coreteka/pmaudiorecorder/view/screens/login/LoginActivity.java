package com.coreteka.pmaudiorecorder.view.screens.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.coreteka.pmaudiorecorder.R;
import com.coreteka.pmaudiorecorder.util.ValidateUtil;
import com.coreteka.pmaudiorecorder.util.layout.annotation.RequiresView;
import com.coreteka.pmaudiorecorder.util.mvp.annotation.RequiresPresenter;
import com.coreteka.pmaudiorecorder.view.base.AbsActivity;
import com.coreteka.pmaudiorecorder.view.screens.audio.RecordActivity;

import butterknife.BindView;

@RequiresView(R.layout.activity_main)
@RequiresPresenter(LoginPresenter.class)
public class LoginActivity extends AbsActivity<LoginPresenter> implements LoginPresenter.View {

    @BindView(R.id.btnSignIn)
    TextView tvSignIn;
    @BindView(R.id.etLogin)
    EditText etLogin;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        translucentStatusBar();
        tvSignIn.setOnClickListener(v -> {
            String login = etLogin.getText().toString();
            String pass = etPassword.getText().toString();
            if (TextUtils.isEmpty(login) || TextUtils.isEmpty(pass)) {
                showError(R.string.error_empty_field);
                return;
            }
            if (!ValidateUtil.validate(login, ValidateUtil.Type.EMAIL)) {
                showError(R.string.error_email_format);
                return;
            }

            getPresenter().login(login, pass);
        });
    }

    @Override
    public void onLoginSuccess() {
        startActivity(new Intent(this, RecordActivity.class));
        finish();
    }

    @Override
    public void onLoginFailed() {
        progressBar.setVisibility(View.INVISIBLE);
        tvSignIn.setEnabled(true);
    }

    @Override
    public void onTokenAbsent() {

    }

    @Override
    public void onEmailFetched(String email) {
        etLogin.setText(email);
    }

    @Override
    public void onStartLogin() {
        progressBar.setVisibility(View.VISIBLE);
        tvSignIn.setEnabled(false);
    }
}
