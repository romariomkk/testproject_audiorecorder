package com.coreteka.pmaudiorecorder.view.base;

import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.widget.EditText;

public class DebounceTextWatcher extends AbsTextWatcher {

    private static final long DEFAULT_DEBOUNCE = 300;

    private final long debounce;
    private final Handler handler = new Handler(Looper.getMainLooper());
    private final EditText editText;
    private final TextChangedListener listener;
    private final NotifyRunnable runnable;

    private DebounceTextWatcher(long debounce, EditText editText, TextChangedListener listener) {
        this.debounce = debounce;
        this.editText = editText;
        this.listener = listener;
        this.runnable = new NotifyRunnable(this.editText, this.listener);
        editText.addTextChangedListener(this);
    }

    public static DebounceTextWatcher create(EditText target,
                                             TextChangedListener listener,
                                             long debounce) {
        return new DebounceTextWatcher(debounce, target, listener);
    }

    public static DebounceTextWatcher create(EditText target,
                                             TextChangedListener listener) {
        return new DebounceTextWatcher(DEFAULT_DEBOUNCE, target, listener);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        super.beforeTextChanged(s, start, count, after);
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        super.onTextChanged(s, start, before, count);
        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(runnable, debounce);
    }

    @Override
    public void afterTextChanged(Editable s) {
        super.afterTextChanged(s);
    }

    private static class NotifyRunnable implements Runnable {
        private final EditText editText;
        private final TextChangedListener listener;

        private NotifyRunnable(EditText editText, TextChangedListener listener) {
            this.editText = editText;
            this.listener = listener;
        }

        @Override
        public void run() {
            if (listener != null && editText != null)
                listener.onTextChanged(editText.getText());
        }
    }

    public interface TextChangedListener {
        void onTextChanged(CharSequence charSequence);
    }
}
