package com.coreteka.pmaudiorecorder.view.screens.audio;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.coreteka.pmaudiorecorder.R;
import com.coreteka.pmaudiorecorder.util.FormatUtil;
import com.coreteka.pmaudiorecorder.util.Keys;
import com.coreteka.pmaudiorecorder.util.UIUtil;
import com.coreteka.pmaudiorecorder.util.layout.annotation.RequiresMenu;
import com.coreteka.pmaudiorecorder.util.layout.annotation.RequiresView;
import com.coreteka.pmaudiorecorder.util.mvp.annotation.RequiresPresenter;
import com.coreteka.pmaudiorecorder.view.base.AbsToolbarActivity;
import com.coreteka.pmaudiorecorder.view.screens.login.LoginActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;

@RequiresMenu(R.menu.logout)
@RequiresView(R.layout.activity_record)
@RequiresPresenter(RecordPresenter.class)
public class RecordActivity extends AbsToolbarActivity<RecordPresenter> implements RecordPresenter.View {

    @BindView(R.id.tvScript)
    TextView tvScript;
    @BindView(R.id.tvCounter)
    TextView tvCounter;
    @BindView(R.id.tvTimer)
    TextView tvTimer;
    @BindView(R.id.btnMain)
    ImageButton btnMain;

    private boolean isRecording = false;
    private boolean areAllPermissionsGranted = false;
    private int backPressCount = 0;

    private int tensOfSingleSecondElapsed;
    private Timer mTimer;
    private Handler mHandler = new Handler(Looper.getMainLooper());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.play);
        setNavigation(R.drawable.ic_microphone_button, v -> {
        });
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        UIUtil.onClick(btnMain, mOnMicClickListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        requestPermissions();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.logout) {
            showDialog(R.string.hint_sure_logout_title, isRecording ? R.string.hint_sure_recording_logout : R.string.hint_sure_logout,
                    () -> {
                        if (isRecording) {
                            getPresenter().finishRecordingOnly();
                        } else {
                            getPresenter().logout();
                        }
                    }
            );
        }
        return super.onOptionsItemSelected(item);
    }

    void requestPermissions() {
        List<String> array = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
            array.add(Manifest.permission.RECORD_AUDIO);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            array.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WAKE_LOCK) != PackageManager.PERMISSION_GRANTED)
            array.add(Manifest.permission.WAKE_LOCK);

        if (!array.isEmpty()) {
            areAllPermissionsGranted = false;
            String[] toRequestArray = array.toArray(new String[]{});
            requestPermissions(toRequestArray, 1996);
        } else {
            areAllPermissionsGranted = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean isGranted = true;
        for (int result : grantResults) {
            isGranted &= result == PackageManager.PERMISSION_GRANTED;
        }
        if (!isGranted) {
            requestPermissions();
            areAllPermissionsGranted = false;
        } else {
            areAllPermissionsGranted = true;
        }
    }

    @Override
    public void onLogoutSuccess() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void onStopRecordingOnlyRequested() {
        onRecordingStatusUpdated(false);
        getPresenter().logout();
    }

    @Override
    public void onRecordingProcessed(Pair<Integer, Boolean> totals) {
        tvCounter.setText(String.format(getString(R.string.title_total_recordings), totals.first));
        if (totals.second) {
            Snackbar.make(tvCounter, R.string.message_sending_success, 3000).show();
        }
    }

    @Override
    public void onRecordingStatusUpdated(Boolean isRecordingNow) {
        isRecording = isRecordingNow;
        if (!isRecording) {
            isRecording = false;
            tensOfSingleSecondElapsed = 0;
            tvTimer.setText(getString(R.string.text_default_timer));
            stopTimer();
        } else {
            startTimer();
        }
    }

    @Override
    public void onBackPressed() {
        if (++backPressCount == 1) {
            Toast.makeText(this, R.string.hint_press_again, Toast.LENGTH_SHORT).show();
        } else {
            super.onBackPressed();
        }
    }

    private void startTimer() {
        stopTimer();
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                updateTimer();
            }
        }, 0, 100);
    }

    private void updateTimer() {
        mHandler.post(() -> {
            if (isRecording) {
                tensOfSingleSecondElapsed += 100;
                tvTimer.setText(FormatUtil.formatTime(tensOfSingleSecondElapsed));
                if (tensOfSingleSecondElapsed == 1000 * Keys.VALUE_TO_START_COUNTDOWN_FROM) { //29000 if value = 29
                    mOnMicClickListener.onClick(btnMain);
                }
            }
        });
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }
    }

    private View.OnClickListener mOnMicClickListener = v -> {
        if (!areAllPermissionsGranted) {
            requestPermissions();
            return;
        }

        btnMain.setImageResource(!isRecording
                ? R.drawable.ic_stop_player_button
                : R.drawable.ic_microphone);

        if (!isRecording) {
            getPresenter().startRecording();
        } else {
            getPresenter().finishRecording();
        }
    };
}
