package com.coreteka.pmaudiorecorder.view.base;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.coreteka.pmaudiorecorder.R;
import com.coreteka.pmaudiorecorder.util.AnnotationUtil;
import com.coreteka.pmaudiorecorder.util.mvp.BaseView;

import butterknife.ButterKnife;

public abstract class AbsFragment<P extends AbsPresenter> extends AbsDaggerFragment<P> implements BaseView {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int menuId = AnnotationUtil.findMenuId(this);
        if (menuId != -1) {
            setHasOptionsMenu(true);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int layoutId = AnnotationUtil.findViewId(this);
        if (layoutId != -1)
            return inflater.inflate(layoutId, container, false);
        else
            return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        int menuId = AnnotationUtil.findMenuId(this);
        if (menuId != -1) {
            inflater.inflate(menuId, menu);
            return;
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void showError(String string, Runnable runnable) {
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.error_title)
                .setMessage(string)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    dialog.dismiss();
                    if (runnable != null) {
                        runnable.run();
                    }
                })
                .show();
    }
    
    @Override
    public void showError(@StringRes int strRes) {
        showError(getString(strRes));
    }
    
    @Override
    public void showError(@StringRes int strRes, @Nullable Runnable runnable) {
        showError(getString(strRes), runnable);
    }
    
    @Override
    public void showToast(int id) {
        showToast(getString(id));
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showNetworkError() {
        showError(R.string.error_network);
    }

    @Override
    public void showTimeOutError() {
        showError(R.string.error_timeout);
    }

    @Override
    public void showSessionTimeout() {
        showError(R.string.error_session_timeout);
    }

    @Override
    public void enableTouch() {
        if (getActivity() != null)
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void disableTouch() {
        if (getActivity() != null)
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
