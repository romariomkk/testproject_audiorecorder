package com.coreteka.pmaudiorecorder.view.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.coreteka.pmaudiorecorder.util.AnnotationUtil;
import com.coreteka.pmaudiorecorder.util.mvp.BasePresenter;
import com.coreteka.pmaudiorecorder.util.mvp.BaseView;
import com.coreteka.pmaudiorecorder.util.mvp.PresenterFactory;

import javax.inject.Inject;

import dagger.android.AndroidInjection;


/**
 * Represents activity that injects presenter factory
 *
 * @param <P>
 */
public abstract class AbsDaggerActivity<P extends AbsPresenter> extends AppCompatActivity
        implements BaseView {

    @Inject
    PresenterFactory factory;

    private P presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inject();
        if (presenter != null)
            presenter.onCreate(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (presenter != null)
            presenter.bindView(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (presenter != null)
            presenter.unbindView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null)
            presenter.onCleanup();
    }

    public P getPresenter() {
        if (presenter == null)
            throw new IllegalStateException("Presenter not found");
        return presenter;
    }


    private void inject() {
        if (AnnotationUtil.hasPresenter(this)) {
            AndroidInjection.inject(this);
            setPresenter();
        }
    }

    private void setPresenter() {
        Class<? extends BasePresenter> presenterClass = AnnotationUtil.findPresenterClass(this);
        if (presenterClass == null)
            throw new IllegalStateException("Presenter class not found");
        presenter = factory.provide(presenterClass);
    }

}
