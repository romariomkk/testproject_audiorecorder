package com.coreteka.pmaudiorecorder.view.base;

import android.text.Editable;
import android.text.TextWatcher;

public interface Validator extends TextWatcher {

    boolean validate();

    Validator subscribeViews();
    Validator unsubscribeViews();

    @Override
    default void afterTextChanged(Editable s){
    }

    @Override
    default void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    default void onTextChanged(CharSequence s, int start, int before, int count) {
    }
}
