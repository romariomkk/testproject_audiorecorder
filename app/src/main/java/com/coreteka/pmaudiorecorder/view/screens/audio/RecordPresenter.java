package com.coreteka.pmaudiorecorder.view.screens.audio;

import android.support.v4.util.Pair;

import com.coreteka.pmaudiorecorder.core.interactors.AudioInteractor;
import com.coreteka.pmaudiorecorder.core.interactors.AuthInteractor;
import com.coreteka.pmaudiorecorder.util.mvp.BaseView;
import com.coreteka.pmaudiorecorder.util.rx.Subscribe;
import com.coreteka.pmaudiorecorder.view.base.AbsPresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.subjects.BehaviorSubject;

public class RecordPresenter extends AbsPresenter<RecordPresenter.View> {

    private final BehaviorSubject<Boolean> isRecordingPublisher = BehaviorSubject.createDefault(false);
    private final BehaviorSubject<Pair<Integer, Boolean>> counterPublisher = BehaviorSubject.createDefault(new Pair<>(0, false));

    private final AuthInteractor authInteractor;
    private final AudioInteractor audioInteractor;

    @Inject
    public RecordPresenter(AuthInteractor authInteractor,
                           AudioInteractor audioInteractor) {
        this.authInteractor = authInteractor;
        this.audioInteractor = audioInteractor;
    }

    @Override
    public void onCreate(View view) {
        super.onCreate(view);
        add(audioInteractor.syncAllUnsynced()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(Subscribe.completable(view, () -> {}, t -> {}))
        );

        add(audioInteractor.totalRecordings()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(Subscribe.single(view, counter -> counterPublisher.onNext(new Pair<>(counter, false))))
        );
    }

    @Override
    public void bindView(View view) {
        super.bindView(view);
        addSubject(isRecordingPublisher.subscribe(view::onRecordingStatusUpdated));
        addSubject(counterPublisher.subscribe(view::onRecordingProcessed));
        counterPublisher.onNext(new Pair<>(counterPublisher.getValue().first, false));
    }

    @Override
    public void onCleanup() {
        super.onCleanup();
        isRecordingPublisher.onComplete();
        counterPublisher.onComplete();
    }

    public void startRecording() {
        add(audioInteractor.startRecording()
                .doOnSubscribe(__ -> isRecordingPublisher.onNext(true))
                .subscribe()
        );
    }

    public void finishRecordingOnly() {
        add(audioInteractor.forceStopRecording()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (isBinded()) {
                        view.onStopRecordingOnlyRequested();
                    }
                    isRecordingPublisher.onNext(false);
                })
                .subscribeWith(Subscribe.completable(view, () -> {}, t -> {}))
        );
    }

    public void finishRecording() {
        add(audioInteractor.stopRecording()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> isRecordingPublisher.onNext(false))
                .subscribeWith(Subscribe.observer(view, counter -> counterPublisher.onNext(new Pair<>(counter, true)), t -> {}))
        );
    }

    public void logout() {
        add(authInteractor.logout()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::onLogoutSuccess, t -> {})
        );
    }

    public interface View extends BaseView {

        void onLogoutSuccess();

        void onStopRecordingOnlyRequested();

        void onRecordingProcessed(Pair<Integer, Boolean> totals);

        void onRecordingStatusUpdated(Boolean isRecording);
    }
}
