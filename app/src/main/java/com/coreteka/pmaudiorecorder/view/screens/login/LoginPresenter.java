package com.coreteka.pmaudiorecorder.view.screens.login;

import com.coreteka.pmaudiorecorder.core.interactors.AuthInteractor;
import com.coreteka.pmaudiorecorder.util.mvp.BaseView;
import com.coreteka.pmaudiorecorder.util.rx.Subscribe;
import com.coreteka.pmaudiorecorder.view.base.AbsPresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class LoginPresenter extends AbsPresenter<LoginPresenter.View> {

    private final AuthInteractor authInteractor;

    @Inject
    public LoginPresenter(AuthInteractor authInteractor) {
        this.authInteractor = authInteractor;
    }

    @Override
    public void onCreate(View view) {
        super.onCreate(view);
        add(authInteractor.isAuthenticated()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::onLoginSuccess, t -> view.onTokenAbsent())
        );
    }

    @Override
    public void bindView(View view) {
        super.bindView(view);
        add(authInteractor.email()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(Subscribe.single(view, view::onEmailFetched))
        );
    }

    public void login(String login, String pass) {
        add(authInteractor.login(login, pass)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> view.onStartLogin())
                .subscribeWith(Subscribe.completableWithError(view,
                        view::onLoginSuccess, t -> view.onLoginFailed()))
        );
    }

    public interface View extends BaseView {
        void onLoginSuccess();

        void onLoginFailed();

        void onTokenAbsent();

        void onEmailFetched(String email);

        void onStartLogin();
    }
}
