package com.coreteka.pmaudiorecorder.view.base;


import android.content.Context;
import android.support.v4.app.Fragment;

import com.coreteka.pmaudiorecorder.util.AnnotationUtil;
import com.coreteka.pmaudiorecorder.util.mvp.BasePresenter;
import com.coreteka.pmaudiorecorder.util.mvp.BaseView;
import com.coreteka.pmaudiorecorder.util.mvp.PresenterFactory;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

public abstract class AbsDaggerFragment<P extends AbsPresenter> extends Fragment implements BaseView {

    @Inject
    PresenterFactory factory;

    private P presenter;

    @Override
    public void onAttach(Context context) {
        inject();
        super.onAttach(context);
        if (presenter != null) {
            presenter.onCreate(this);
        }
    }

    private void inject() {
        if (AnnotationUtil.hasPresenter(this)) {
            AndroidSupportInjection.inject(this);
            setPresenter();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (presenter != null)
            presenter.bindView(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (presenter != null)
            presenter.unbindView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (presenter != null)
            presenter.onCleanup();
    }

    private void setPresenter() {
        Class<? extends BasePresenter> presenterClass = AnnotationUtil.findPresenterClass(this);
        if (presenterClass == null)
            throw new IllegalStateException("Presenter class not set");
        presenter = factory.provide(presenterClass);
    }

    public P getPresenter() {
        if (presenter == null)
            throw new IllegalStateException("Presenter not found");
        return presenter;
    }
}
