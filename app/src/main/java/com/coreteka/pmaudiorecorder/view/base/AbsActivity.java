package com.coreteka.pmaudiorecorder.view.base;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.coreteka.pmaudiorecorder.R;
import com.coreteka.pmaudiorecorder.util.AnnotationUtil;

import butterknife.ButterKnife;

/**
 * Represents activity view layer
 *
 * @param <P> Abs Presenter
 */
public abstract class AbsActivity<P extends AbsPresenter> extends AbsDaggerActivity<P> {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView();
        ButterKnife.bind(this);
    }

    @Override
    public void showDialog(int title, int message, Runnable... runnables) {
        showDialog(getString(title), getString(message), runnables);
    }

    @Override
    public void showDialog(int title, String message, Runnable... runnables) {
        showDialog(getString(title), message, runnables);
    }

    @Override
    public void showDialog(String title, String message, Runnable... runnables) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setNegativeButton(R.string.cancel, (dialog, which) -> {
                    if (dialog != null) dialog.dismiss();
                })
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    dialog.dismiss();
                    if (runnables != null) {
                        for (Runnable runnable : runnables) {
                            runnable.run();
                        }
                    }
                })
                .show();
    }

    @Override
    public void showError(String string, @Nullable Runnable runnable) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.error_title)
                .setMessage(string)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    dialog.dismiss();
                    if (runnable != null)
                        runnable.run();
                })
                .show();
    }
    
    @Override
    public void showError(String str) {
        showError(str, null);
    }

    @Override
    public void showError(@StringRes int strRes) {
        showError(getString(strRes), null);
    }
    
    @Override
    public void showError(@StringRes int strRes, @Nullable Runnable runnable) {
        showError(getString(strRes), runnable);
    }
    
    @Override
    public void showNetworkError() {
        showError(R.string.error_network);
    }

    @Override
    public void showTimeOutError() {
        showError(R.string.error_timeout);
    }

    @Override
    public void showSessionTimeout() {
        showError(R.string.error_session_timeout);//todo refresh token
    }

    @Override
    public void showToast(int id) {
        showToast(getString(id));
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void enableTouch() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void disableTouch() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void setView() {
        int layoutId = AnnotationUtil.findViewId(this);
        if (layoutId != -1)
            setContentView(layoutId);
    }

    public void fade() {
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void fadeFinish() {
        finish();
        fade();
    }

    public void translucentStatusBar() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }
}
