package com.coreteka.pmaudiorecorder;

import android.app.Activity;
import android.app.Application;

import com.coreteka.pmaudiorecorder.di.AppComponent;
import com.coreteka.pmaudiorecorder.di.DaggerAppComponent;
import com.facebook.stetho.Stetho;

import javax.inject.Inject;

import cafe.adriel.androidaudioconverter.AndroidAudioConverter;
import cafe.adriel.androidaudioconverter.callback.ILoadCallback;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import timber.log.Timber;

public class PMAudioRecorderApp extends Application implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> androidInjector;

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .application(this)
                .build();
        appComponent.inject(this);

        Timber.plant(new Timber.DebugTree());
        AndroidAudioConverter.load(this, new ILoadCallback() {
            @Override
            public void onSuccess() {
                Timber.d("ffmpeg loaded");
            }

            @Override
            public void onFailure(Exception e) {
                Timber.e("ffmpeg failed to load");
            }
        });
        Stetho.initializeWithDefaults(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return androidInjector;
    }
}
