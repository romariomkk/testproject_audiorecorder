package com.coreteka.pmaudiorecorder.api;

import com.coreteka.pmaudiorecorder.api.request.AuthRequest;
import com.coreteka.pmaudiorecorder.api.response.TokenResponse;

import io.reactivex.Completable;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface PMApi {

    @POST("authenticate")
    @Headers("Content-Type: application/json")
    Observable<TokenResponse> login(@Body AuthRequest authBody);

    @Multipart
    @POST("speechRecords")
    Completable sendAudio(@Part MultipartBody.Part file);

}
