package com.coreteka.pmaudiorecorder.api;

import android.text.TextUtils;

import com.coreteka.pmaudiorecorder.core.storage.AuthStorage;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class TokenInterceptor implements Interceptor {

    private final AuthStorage authStorage;

    public TokenInterceptor(AuthStorage authStorage) {
        this.authStorage = authStorage;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        return chain.proceed(addToken(chain));
    }

    private Request addToken(Chain chain) {
        final String token = authStorage.rawToken();

        Request.Builder requestBuilder = chain.request().newBuilder();
        if (!TextUtils.isEmpty(token)) {
            requestBuilder.addHeader("x-auth-token", token);
        }
        return requestBuilder.build();
    }
}
