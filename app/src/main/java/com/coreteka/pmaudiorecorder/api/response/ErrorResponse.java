package com.coreteka.pmaudiorecorder.api.response;

public class ErrorResponse {

    private String status;
    private String errorCode;
    private String errorMessage;

    public String getStatus() {
        return status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
